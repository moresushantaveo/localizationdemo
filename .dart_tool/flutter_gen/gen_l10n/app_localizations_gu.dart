


import 'app_localizations.dart';

/// The translations for Gujarati (`gu`).
class AppLocalizationsGu extends AppLocalizations {
  AppLocalizationsGu([String locale = 'gu']) : super(locale);

  @override
  String get name => 'પૂરું નામ';

  @override
  String get address => 'સરનામું';

  @override
  String get state => 'રાજ્ય';

  @override
  String get city => 'શહેર';

  @override
  String get url => 'સમાન સ્ત્રોત નિર્ધારણ';

  @override
  String get media => 'મીડિયા અપલોડ કરો';

  @override
  String get comments => 'ટિપ્પણીઓ';

  @override
  String get submit => 'સબમિટ કરો';
}
