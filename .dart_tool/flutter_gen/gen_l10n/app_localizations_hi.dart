


import 'app_localizations.dart';

/// The translations for Hindi (`hi`).
class AppLocalizationsHi extends AppLocalizations {
  AppLocalizationsHi([String locale = 'hi']) : super(locale);

  @override
  String get name => 'पूरा नाम';

  @override
  String get address => 'पता';

  @override
  String get state => 'राज्य';

  @override
  String get city => 'शहर';

  @override
  String get url => 'यूआरएल';

  @override
  String get media => 'मीडिया अपलोड करें';

  @override
  String get comments => 'टिप्पणियाँ';

  @override
  String get submit => 'सबमिट करे';
}
