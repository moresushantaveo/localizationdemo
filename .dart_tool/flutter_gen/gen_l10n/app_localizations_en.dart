


import 'app_localizations.dart';

/// The translations for English (`en`).
class AppLocalizationsEn extends AppLocalizations {
  AppLocalizationsEn([String locale = 'en']) : super(locale);

  @override
  String get name => 'Full Name';

  @override
  String get address => 'Address';

  @override
  String get state => 'State';

  @override
  String get city => 'City';

  @override
  String get url => 'Url';

  @override
  String get media => 'Upload Media';

  @override
  String get comments => 'Comments';

  @override
  String get submit => 'Submit';
}
