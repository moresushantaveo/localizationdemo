mkdir lib/l10n

echo DOWNLOADING ALL LANGUAGES

curl --location --request GET 'https://localise.biz/api/export/locale/en.arb?key=g8nzSVHjvFJ8sbdTGafORR7DzU8CmFHO' > lib/l10n/intl_en.arb
echo ENGLISH

curl --location --request GET 'https://localise.biz/api/export/locale/gu.arb?key=g8nzSVHjvFJ8sbdTGafORR7DzU8CmFHO' > lib/l10n/intl_gu.arb
echo GUJRATI

curl --location --request GET 'https://localise.biz/api/export/locale/hi.arb?key=g8nzSVHjvFJ8sbdTGafORR7DzU8CmFHO' > lib/l10n/intl_hi.arb
echo HINDI

echo CREATING l10n.yaml FILE

echo "arb-dir: lib/l10n
template-arb-file: intl_en.arb
output-localization-file: app_localizations.dart" > l10n.yaml

if grep "generate" pubspec.yaml > /dev/null
then
    # code if found
    echo ""
else
    echo "  generate: true" >> pubspec.yaml
fi

flutter gen-l10n