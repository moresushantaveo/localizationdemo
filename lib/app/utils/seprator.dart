import 'package:flutter/material.dart';

class VSeparator {
  static Widget small() {
    return const SizedBox(
      height: 10,
    );
  }

  static Widget medium() {
    return const SizedBox(
      height: 20,
    );
  }

  static Widget large() {
    return const SizedBox(
      height: 30,
    );
  }

  static Widget custom(double height) {
    return SizedBox(
      height: height,
    );
  }
}

class HSeparator {
  static Widget small() {
    return const SizedBox(
      width: 10,
    );
  }

  static Widget medium() {
    return const SizedBox(
      width: 20,
    );
  }

  static Widget large() {
    return const SizedBox(
      width: 30,
    );
  }

  static Widget custom(double width) {
    return SizedBox(
      width: width,
    );
  }
}
