import 'package:flutter/material.dart';

class FieldText extends StatelessWidget {
  final String fieldName;
  final Function(String) onFiledSubmitted;
  final TextEditingController fieldController;
  final FocusNode focusNode;
  final FocusNode? nextFocusNode;
  final String? Function(String?)? validator;
  final void Function(String)? onChanged;
  final int textLines;
  const FieldText(
      {Key? key,
      this.textLines = 1,
      required this.fieldName,
      required this.onFiledSubmitted,
      required this.fieldController,
      required this.focusNode,
      this.nextFocusNode,
      this.validator,
      this.onChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(fieldName),
        TextFormField(
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'Field cannot be empty';
            } else {
              if (validator != null) {
                validator!(value);
              }
              return null;
            }
          },
          autocorrect: true,
          onFieldSubmitted: (v) {
            onFiledSubmitted(v);
            if (nextFocusNode != null) {
              FocusScope.of(context).requestFocus(nextFocusNode);
            }
          },
          // style: getHeadingTextStyle(
          //     context: context,
          //     color: Theme.of(context).colorScheme.onSurface),
          focusNode: focusNode,
          decoration: InputDecoration(
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Theme.of(context).colorScheme.secondary,
              ),
            ),
          ),
          cursorWidth: 5.0,
          cursorColor: Theme.of(context).colorScheme.secondary,
          onChanged: onChanged,
          controller: fieldController,
          minLines: textLines,
          maxLines: textLines,
        ),
      ],
    );
  }
}
