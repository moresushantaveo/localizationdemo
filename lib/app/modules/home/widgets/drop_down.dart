import 'package:flutter/material.dart';

class CustomDropDown extends StatelessWidget {
  final Object? value;
  final List<DropdownMenuItem<Object>>? items;
  final void Function(Object?)? onChanged;
  final String? fieldName;
  final FocusNode focusNode;
  final FocusNode? nextFocusNode;
  final double? width;
  const CustomDropDown(
      {Key? key,
      this.value,
      this.items,
      this.onChanged,
      this.fieldName,
      required this.focusNode,
      this.nextFocusNode, this.width})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Visibility(
          visible: fieldName != null,
          child: Text(fieldName ?? ''),
        ),
        SizedBox(
          width: width ?? double.infinity,
          child: DropdownButton(
            focusNode: focusNode,
            value: value,
            items: items,
            onChanged: (value) {
              onChanged;
              if (nextFocusNode != null) {
                FocusScope.of(context).requestFocus(nextFocusNode);
              }
            },
          ),
        ),
      ],
    );
  }
}
