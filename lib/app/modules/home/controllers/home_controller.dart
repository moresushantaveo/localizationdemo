import 'dart:typed_data';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  Map<String, String> locales = {
    'en': "English",
    'gu': "Gujrati",
    'hi': "Hindi",
  };
  RxString selectedLocale = ''.obs;
  RxString selectedCity = ''.obs;
  RxString selectedState = ''.obs;

  List<DropdownMenuItem<String>> items = <DropdownMenuItem<String>>[].obs;
  Rx<TextEditingController> fullNameTextController =
      TextEditingController().obs;
  Rx<TextEditingController> addressTextController = TextEditingController().obs;
  Rx<TextEditingController> urlTextController = TextEditingController().obs;
  Rx<TextEditingController> commentsTextController =
      TextEditingController().obs;

  FocusNode languageFocusNode = FocusNode();
  FocusNode fullNameFocusNode = FocusNode();
  FocusNode addressFocusNode = FocusNode();
  FocusNode stateFocusNode = FocusNode();
  FocusNode cityFocusNode = FocusNode();
  FocusNode urlFocusNode = FocusNode();
  FocusNode commentsFocusNode = FocusNode();
  FilePickerResult? result;
  uploadFiles() async {
    result = await FilePicker.platform.pickFiles(allowMultiple: true);
    if (result != null) {
      try {
        Uint8List uploadfile = result!.files.single.bytes!;
        String filename = result!.files.single.name;
        print(filename);
      } catch (e) {
        print(e);
      }
    } else {}
  }

  @override
  void onInit() {
    locales.forEach((key, value) {
      items.add(DropdownMenuItem(
        value: key,
        child: Text(value),
      ));
    });
    selectedLocale.value = items[0].value!;
    super.onInit();
  }

  void changeLocale(String locale) {
    selectedLocale.value = locale;
    Get.updateLocale(Locale(locale));
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
