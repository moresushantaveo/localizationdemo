import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:get/get.dart';
import 'package:localization_demo/app/modules/home/widgets/drop_down.dart';
import 'package:localization_demo/app/modules/home/widgets/text_fields.dart';
import 'package:localization_demo/app/utils/seprator.dart';
import 'package:lottie/lottie.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sign Up'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Container(
          alignment: Alignment.center,
          margin: const EdgeInsets.all(16.0),
          child: Card(
              elevation: 18,
              child: Container(
                width: 400,
                padding: const EdgeInsets.all(28),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Obx(
                      () => Align(
                        alignment: Alignment.centerRight,
                        child: CustomDropDown(
                          width: 100,
                          value: controller.selectedLocale.value,
                          items: controller.items,
                          focusNode: controller.languageFocusNode,
                          onChanged: (value) {
                            controller.changeLocale(value.toString());
                          },
                        ),
                      ),
                    ),
                    Lottie.asset('assets/animations/register.json'),
                    VSeparator.medium(),
                    FieldText(
                      fieldName: AppLocalizations.of(context)!.name,
                      fieldController: controller.fullNameTextController.value,
                      onFiledSubmitted: (value) {},
                      focusNode: controller.fullNameFocusNode,
                      nextFocusNode: controller.addressFocusNode,
                    ),
                    VSeparator.medium(),
                    FieldText(
                      textLines: 4,
                      fieldName: AppLocalizations.of(context)!.address,
                      fieldController: controller.addressTextController.value,
                      onFiledSubmitted: (value) {},
                      focusNode: controller.addressFocusNode,
                    ),
                    VSeparator.medium(),
                    CustomDropDown(
                      fieldName: AppLocalizations.of(context)!.state,
                      value: controller.selectedLocale.value,
                      items: controller.items,
                      focusNode: controller.stateFocusNode,
                      onChanged: (value) {
                        controller.selectedState.value = value.toString();
                      },
                    ),
                    VSeparator.medium(),
                    CustomDropDown(
                      fieldName: AppLocalizations.of(context)!.city,
                      value: controller.selectedLocale.value,
                      items: controller.items,
                      focusNode: controller.cityFocusNode,
                      onChanged: (value) {
                        controller.selectedCity.value = value.toString();
                      },
                    ),
                    VSeparator.medium(),
                    FieldText(
                      fieldName:
                          '${AppLocalizations.of(context)!.url} (Optional)',
                      fieldController: controller.urlTextController.value,
                      onFiledSubmitted: (value) {},
                      focusNode: controller.urlFocusNode,
                    ),
                    VSeparator.medium(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                            '${AppLocalizations.of(context)!.media} (Optional)'),
                        HSeparator.medium(),
                        GestureDetector(
                          onTap: () => controller.uploadFiles(),
                          child: Image.asset(
                            'assets/images/upload.png',
                            height: 15,
                            width: 15,
                          ),
                        ),
                      ],
                    ),
                    VSeparator.medium(),
                    FieldText(
                      fieldName:
                          '${AppLocalizations.of(context)!.comments} (Optional)',
                      fieldController: controller.commentsTextController.value,
                      onFiledSubmitted: (value) {},
                      focusNode: controller.commentsFocusNode,
                      textLines: 4,
                    ),
                    VSeparator.medium(),
                    ElevatedButton(
                      onPressed: () {
                        print(AppLocalizations.supportedLocales.toList());
                      },
                      style: ButtonStyle(
                        shape: MaterialStateProperty.all(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(24),
                          ),
                        ),
                      ),
                      child: Container(
                        width: double.infinity,
                        padding: const EdgeInsets.all(16),
                        child: Text(
                          AppLocalizations.of(context)!.submit,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    )
                  ],
                ),
              )),
        ),
      ),
    );
  }
}
